DialogueActions is a loader mod that allows you to access more game actions from within your dialogue.
There are three ways in which DialogueActions gives you more access.

Firstly, DialogueActions adds numerous triggers that you can use. 
You can use these triggers just like you would use any other triggers.
An example:
resistance:"[GAG]hurk"

A full list of triggers can be found in Triggers.txt.

Secondly, DialogueActions adds more default lines that will trigger based on certain conditions.
You can add these lines to your dialogue like you would add custom lines.
An example:
start:"This is the first line being spoken."

A full list of linetypes can be found in Linetypes.txt.

Thirdly, DialogueActions allows setting clothes via the "set" line-attribute.
Use of DialogueActions will lead to the variables that DialogueActions uses for the clothes being deleted, though.
This means you can't have any of your variables named like the ones DialogueActions uses for the clothing.
The clothing variables accept the same values as are available in vanilla SDT (example top: ["None", "Bra", "Bikini"]). 
If you don't provide RGB values, the colour will not change. 
If you do provide them, they have to match with the listed amount of values (for example "3 RGB values" means three times a number from 0 to 255, 4 RGBA values means three RGB values, one alpha value from 0 to 1). 
An example:
resistance:"Let me get rid of these pants."{"set":{"da.clothes.bottoms.type":"None"}}
drool:"My *da.clothes.tops.type* is turning transparent!"{"set":{"da.clothes.tops.a":"-0.5"}}

A full list of the variables can be found in Variables.txt.

Part of DialogueActions is VariableArithmetic. It allows you to perform arithmetic with variables.
An example:
initial_settings:{"var1":10,"var2":8}
intro:"I'm *var1 + var2* years old!"
will turn into "I'm 18 years old!"

A full list of operators and explanations can be found in Operators.txt.

Additionally, DialogueActions will instantly play lines that only contain a single trigger.
This means that if you have something like a callback or a branch decision, there will no longer be a lengthy period of blank box visible to the user.
Variables will still be set. If other mods relied on reading those lines, they can no longer do so - DialogueActions waits until is finished loading the variable values before forcing SDT to play the next line.

For larger lines with multiple triggers, you can add "_INSTANT" to the line name, like "intro4_INSTANT".
If a "_INSTANT" line contains something else than triggers, it won't be played instantly.