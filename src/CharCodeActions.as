package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class CharCodeActions extends CustomTriggerHolder
	{
		private var charCodeToLoad:String = null;
		private var hasLineListener:Boolean = false;
		public function CharCodeActions(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableWrite("da.charcode.load", new FunctionObject(setCharCode, this, []));
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("LOAD_CHARCODE", 0, new FunctionObject(loadCharCode, this, []));
			t.registerTrigger("LOAD_CHARCODE2", 0, new FunctionObject(loadCharCode2, this, []));
			
			t.registerTrigger("LOAD_FULL_CHARCODE", 0, new FunctionObject(loadFullCharCode, this, []));
		}
		
		public function setCharCode(charCode:String):void {
			charCodeToLoad = charCode;
			m.addLineChangeListener(new FunctionObject(onLineChange, this, []));
			hasLineListener = true;
		}
		
		public function onLineChange(...args):void {
			if (g.dialogueControl.sayingPhrase.indexOf("[LOAD_CHARCODE]") == -1 
			&& g.dialogueControl.sayingPhrase.indexOf("[LOAD_CHARCODE2]") == -1
			&& g.dialogueControl.sayingPhrase.indexOf("[LOAD_FULL_CHARCODE]") == -1) {
				loadCharCode();
			}
			hasLineListener = false;
		}
		public function loadCharCode2(...args):void{
			if (charCodeToLoad != null) {
				g.inGameMenu.loadData(g.inGameMenu.getSaveDataString()+";"+charCodeToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No charcode set for loading. (Error: [LOAD_CHARCODE2] without da.charcode.load set)");
			}
			charCodeToLoad = null;
		}
		public function loadCharCode(...args):void{
			//g.inGameMenu.loadData(g.inGameMenu.getSaveDataString()+";"+charCode);
			if (charCodeToLoad != null) {
				loadCharCodeWithSDTCode(charCodeToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No charcode set for loading. (Error: [LOAD_CHARCODE] without da.charcode.load set)");
			}
			charCodeToLoad = null;
		}
		
		public function loadFullCharCode(...args):void {
			if (charCodeToLoad != null) {
				g.inGameMenu.loadData(charCodeToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No charcode set for loading. (Error: [LOAD_FULL_CHARCODE] without da.charcode.load set)");
			}
			charCodeToLoad = null;
		}
		
		public function loadCharCodeWithSDTCode(param1:String) : void
		{
			var _loc_7:* = undefined;
			var _loc_8:* = null;
			var _loc_9:* = null;
			var _loc_10:* = null;
			var _loc_11:* = null;
			var _loc_12:* = null;
			var _loc_13:* = null;
			var _loc_14:* = null;
			var _loc_15:* = null;
			var _loc_16:* = null;
			var _loc_17:* = null;
			var _loc_18:* = null;
			var _loc_19:* = null;
			var _loc_20:* = null;
			var _loc_21:* = null;
			var _loc_22:* = null;
			var _loc_23:* = null;
			var _loc_24:* = null;
			var _loc_25:* = null;
			var _loc_26:* = 0;
			var _loc_27:* = null;
			var _loc_28:* = null;
			var _loc_29:* = null;
			var _loc_30:* = null;
			var _loc_31:* = undefined;
			var _loc_32:* = 0;
			var _loc_5:* = param1.split(";");
			var _loc_6:* = new Array();
			for each (_loc_7 in _loc_5)
			{
				
				_loc_6.push(_loc_7.split(":"));
			}
			_loc_9 = "";
			_loc_10 = "";
			_loc_11 = "";
			_loc_12 = "";
			_loc_13 = "";
			_loc_14 = new Array();
			
			/*g.characterControl.gagControl.findName("none");
			g.characterControl.legwearControl.findName("None");
			g.characterControl.legwearBControl.findName("None");
			g.characterControl.bottomsControl.findName("None");
			g.characterControl.ankleCuffsControl.findName("None");
			g.characterControl.topControl.findName("None");
			g.characterControl.headwearControl.findName("None");
			g.characterControl.armwearControl.findName("none");
			g.characterControl.pantiesControl.findName("none");
			g.characterControl.braControl.findName("none");
			g.characterControl.footwearControl.findName("none");
			g.characterControl.tonguePiercingControl.findName("none");
			g.characterControl.nipplePiercingControl.findName("none");
			g.characterControl.bellyPiercingControl.findName("none");
			g.characterControl.earringControl.findName("none");*
			g.her.tan.setTan(0);*/
			if (!g.inGameMenu.onlyLoadCostume)
			{
				/*g.her.setDefaultBodyScale();
				g.characterControl.setNose(0);
				g.characterControl.setEyebrows(0);
				g.characterControl.setEar(0);*/
				g.inGameMenu.updateBodySlider();
				g.inGameMenu.updateArmsList();
			}
			if (!g.inGameMenu.onlyLoadCostume)
			{
				for each (_loc_8 in _loc_6)
				{
					
					switch(_loc_8[0])
					{
						case "charName":
						{
							g.characterControl.currentName = _loc_8[1];
							break;
						}
						case "mood":
						{
							g.her.setMood(_loc_8[1]);
							g.inGameMenu.updateMoods();
							break;
						}
						case "customHair":
						{
							_loc_9 = _loc_8.slice(1).join(":");
							break;
						}
						case "customBG":
						{
							_loc_11 = _loc_8.slice(1).join(":");
							break;
						}
						case "bodyScale":
						{
							g.her.setBodyScale(Number(_loc_8[1]));
							g.inGameMenu.updateBodySlider();
							break;
						}
						case "arms":
						{
							_loc_15 = _loc_8[1].split(",");
							if (_loc_15.length == 1)
							{
								g.her.findLeftArmPosition(_loc_15[0]);
								g.her.findRightArmPosition(_loc_15[0]);
							}
							else if (_loc_15.length == 2)
							{
								g.her.findLeftArmPosition(_loc_15[0]);
								g.her.findRightArmPosition(_loc_15[1]);
							}
							g.inGameMenu.updateArmsList();
							break;
						}
						case "throatResist":
						{
							g.her.setThroatResistance(Math.max(0, Math.min(100, Number(_loc_8[1]) || 0)));
							g.inGameMenu.updateThroatResistanceSlider();
							break;
						}
						case "hair":
						{
							_loc_10 = _loc_8[1];
							break;
						}
						case "iris":
						{
							_loc_16 = _loc_8[1].split(",");//_loc_10.split(",");
							g.characterControl.findIris(String(_loc_16[0]));
							if (_loc_16.length == 5)
							{
								_loc_16[1] = Math.round(Math.max(0, Math.min(255, Number(_loc_16[1]))));
								_loc_16[2] = Math.round(Math.max(0, Math.min(255, Number(_loc_16[2]))));
								_loc_16[3] = Math.round(Math.max(0, Math.min(255, Number(_loc_16[3]))));
								_loc_16[4] = Math.max(0, Math.min(1, Number(_loc_16[4])));
								if (!isNaN(_loc_16[1]) && !isNaN(_loc_16[2]) && !isNaN(_loc_16[3]) && !isNaN(_loc_16[4]))
								{
									g.characterControl.setIrisFill({r:_loc_16[1], g:_loc_16[2], b:_loc_16[3], a:_loc_16[4]});
								}
							}
							break;
						}
						case "nose":
						{
							g.characterControl.findNose(_loc_8[1]);
							break;
						}
						case "ear":
						{
							g.characterControl.findEar(_loc_8[1]);
							break;
						}
						case "breasts":
						{
							g.characterControl.setBreasts(Math.max(1, Math.min(149, uint(_loc_8[1]))));
							break;
						}
						case "skin":
						{
							g.characterControl.findSkin(_loc_8[1]);
							break;
						}
						case "sclera":
						{
							g.inGameMenu.checkAndLoadRGB(_loc_8[1], g.characterControl.setSclera);
							break;
						}
						case "eyebrow":
						{
							_loc_13 = _loc_8[1];
							break;
						}
						case "hairhsl":
						{
							//_loc_17 = _loc_10.split(",");
							_loc_17 = _loc_8[1].split(",");
							if (_loc_8[1] == "0,1,1,1")
							{
								g.characterControl.removeHairHSL();
							}
							else if (_loc_17.length == 4)
							{
								_loc_17[0] = Math.round(Math.max(-180, Math.min(180, Number(_loc_17[0]))));
								_loc_17[1] = Math.max(0, Math.min(4, Number(_loc_17[1])));
								_loc_17[2] = Math.max(0, Math.min(4, Number(_loc_17[2])));
								_loc_17[3] = Math.max(0, Math.min(4, Number(_loc_17[3])));
								if (!isNaN(_loc_17[0]) && !isNaN(_loc_17[1]) && !isNaN(_loc_17[2]) && !isNaN(_loc_17[3]))
								{
									g.characterControl.applyHairHSL({h:_loc_17[0], s:_loc_17[1], l:_loc_17[2], c:_loc_17[3]});
								}
							}
							break;
						}
						case "skinhsl":
						{
							_loc_18 = _loc_8[1].split(",");//_loc_10.split(",");
							if (_loc_8[1] == "0,1,1,1")
							{
								g.characterControl.removeSkinHSL();
							}
							else if (_loc_18.length == 4)
							{
								_loc_18[0] = Math.round(Math.max(-180, Math.min(180, Number(_loc_18[0]))));
								_loc_18[1] = Math.max(0, Math.min(4, Number(_loc_18[1])));
								_loc_18[2] = Math.max(0, Math.min(4, Number(_loc_18[2])));
								_loc_18[3] = Math.max(0, Math.min(4, Number(_loc_18[3])));
								if (!isNaN(_loc_18[0]) && !isNaN(_loc_18[1]) && !isNaN(_loc_18[2]) && !isNaN(_loc_18[3]))
								{
									g.characterControl.applySkinHSL({h:_loc_18[0], s:_loc_18[1], l:_loc_18[2], c:_loc_18[3]});
								}
							}
							break;
						}
						case "hisskinhsl":
						{
							_loc_19 = _loc_8[1].split(",");//_loc_10.split(",");
							if (_loc_8[1] == "0,1,1,1")
							{
								g.characterControl.removeHisSkinHSL();
							}
							else if (_loc_19.length == 4)
							{
								_loc_19[0] = Math.round(Math.max(-180, Math.min(180, Number(_loc_19[0]))));
								_loc_19[1] = Math.max(0, Math.min(4, Number(_loc_19[1])));
								_loc_19[2] = Math.max(0, Math.min(4, Number(_loc_19[2])));
								_loc_19[3] = Math.max(0, Math.min(4, Number(_loc_19[3])));
								if (!isNaN(_loc_19[0]) && !isNaN(_loc_19[1]) && !isNaN(_loc_19[2]) && !isNaN(_loc_19[3]))
								{
									g.characterControl.applyHisSkinHSL({h:_loc_19[0], s:_loc_19[1], l:_loc_19[2], c:_loc_19[3]});
								}
							}
							break;
						}
						case "bg":
						{
							_loc_12 = _loc_8[1];
							break;
						}
						case "him":
						{
							_loc_20 = _loc_8[1].split(",");//_loc_10.split(",");
							g.him.loadLegacyData(_loc_20);
							break;
						}
						case "dialogue":
						{
							_loc_21 = _loc_8[1];
							g.dialogueControl.loadCustomDialogue(_loc_21, true);
							break;
						}
						default:
						{
							break;
						}
					}
				}
			}
			g.him.loadDataPairs(_loc_6);
			g.her.loadDataPairs(_loc_6);
			for each (_loc_8 in _loc_6)
			{
				
				switch(_loc_8[0])
				{
					case "swfMod":
					{
						_loc_14.push(_loc_8.slice(1).join(":"));
						break;
					}
					case "lipstick":
					{
						_loc_22 = _loc_8[1].split(",");//_loc_10.split(",");
						if (_loc_22.length > 0)
						{
							g.smearLipstick = Boolean(Number(_loc_22[0]));
							g.inGameMenu.updateLipstickSmearing();
						}
						if (_loc_22.length == 5)
						{
							_loc_22[1] = Math.round(Math.max(0, Math.min(255, Number(_loc_22[1]))));
							_loc_22[2] = Math.round(Math.max(0, Math.min(255, Number(_loc_22[2]))));
							_loc_22[3] = Math.round(Math.max(0, Math.min(255, Number(_loc_22[3]))));
							_loc_22[4] = Math.max(0, Math.min(1, Number(_loc_22[4])));
							if (!isNaN(_loc_22[1]) && !isNaN(_loc_22[2]) && !isNaN(_loc_22[3]) && !isNaN(_loc_22[4]))
							{
								g.characterControl.setLipstick({r:_loc_22[1], g:_loc_22[2], b:_loc_22[3], a:_loc_22[4]});
							}
						}
						break;
					}
					case "eyeshadow":
					{
						g.inGameMenu.checkAndLoadRGB(_loc_8[1], g.characterControl.setEyeShadow);
						break;
					}
					case "blush":
					{
						g.inGameMenu.checkAndLoadRGB(_loc_8[1], g.characterControl.setBlush);
						break;
					}
					case "freckles":
					{
						_loc_23 = _loc_8[1].split(",");//_loc_10.split(",");
						g.inGameMenu.checkAndLoadRGB(_loc_23.slice(0, 4).join(","), g.characterControl.setFreckles);
						if (_loc_23[4])
						{
							g.characterControl.setFreckleAmount(Number(_loc_23[(_loc_23.length - 1)]) || 0);
							g.inGameMenu.updateFrecklesSlider();
						}
						break;
					}
					case "mascara":
					{
						_loc_24 = _loc_8[1].split(",");//_loc_10.split(",");
						g.inGameMenu.checkAndLoadRGB(_loc_24.slice(0, 4).join(","), g.characterControl.setMascara);
						if (_loc_24[4])
						{
							_loc_26 = uint(_loc_24[4]);
							_loc_26 = Math.min(100, _loc_26);
							g.mascaraAmount = _loc_26;
							g.inGameMenu.updateMascaraSlider();
						}
						break;
					}
					case "nailpolish":
					{
						g.inGameMenu.checkAndLoadRGB(_loc_8[1], g.characterControl.setNailPolish);
						break;
					}
					case "collar":
					{
						g.characterControl.collarControl.loadDataString(_loc_8[1]);
						break;
					}
					case "cuffs":
					{
						g.characterControl.cuffsControl.loadDataString(_loc_8[1]);
						break;
					}
					case "ankleCuffs":
					{
						g.characterControl.ankleCuffsControl.loadDataString(_loc_8[1]);
						break;
					}
					case "gag":
					{
						g.characterControl.gagControl.loadDataString(_loc_8[1]);
						break;
					}
					case "panties":
					{
						_loc_25 = _loc_8[1].split(",");//_loc_10.split(",");
						if (_loc_25[0] == "shorts")
						{
							_loc_25[0] = "bikeshorts";
							g.characterControl.bottomsControl.loadDataString(_loc_25.join(","));
						}
						else
						{
							g.characterControl.pantiesControl.loadDataString(_loc_8[1]);
						}
						break;
					}
					case "top":
					{
						g.characterControl.braControl.loadDataString(_loc_8[1]);
						break;
					}
					case "armwear":
					{
						g.characterControl.armwearControl.loadDataString(_loc_8[1]);
						break;
					}
					case "bottoms":
					{
						g.characterControl.bottomsControl.loadDataString(_loc_8[1]);
						break;
					}
					case "tops":
					{
						g.characterControl.topControl.loadDataString(_loc_8[1]);
						break;
					}
					case "legwear":
					{
						g.characterControl.legwearControl.loadDataString(_loc_8[1]);
						break;
					}
					case "legwearB":
					{
						g.characterControl.legwearBControl.loadDataString(_loc_8[1]);
						break;
					}
					case "footwear":
					{
						g.characterControl.footwearControl.loadDataString(_loc_8[1]);
						break;
					}
					case "eyewear":
					{
						g.characterControl.eyewearControl.loadDataString(_loc_8[1]);
						break;
					}
					case "headwear":
					{
						_loc_8[1] = (_loc_8[1] as String).replace("zeldastiara", "zeldascirclet");
						g.characterControl.headwearControl.loadDataString(_loc_8[1]);
						break;
					}
					case "tonguePiercing":
					{
						g.characterControl.tonguePiercingControl.loadDataString(_loc_8[1]);
						break;
					}
					case "nipplePiercing":
					{
						g.characterControl.nipplePiercingControl.loadDataString(_loc_8[1]);
						break;
					}
					case "bellyPiercing":
					{
						g.characterControl.bellyPiercingControl.loadDataString(_loc_8[1]);
						break;
					}
					case "earring":
					{
						g.characterControl.earringControl.loadDataString(_loc_8[1]);
						break;
					}
					default:
					{
						break;
					}
				}
			}
			g.her.tan.loadDataPairs(_loc_6);
			if (_loc_14.length > 0)
			{
				g.customElementLoader.tryToLoadSwfMods(_loc_14, true, g.inGameMenu.onlyLoadCostume);
			}
			if (_loc_9 != "")
	
			{
				_loc_27 = g.customElementLoader.tryToLoadHairFile(_loc_9);
				
			}
			else if (_loc_10 != "")
			{
				g.customElementLoader.clearCustomHair();
				_loc_28 = _loc_10.split(",");
				g.characterControl.findHair(_loc_28[0], false);
				g.characterControl.setCurrentElements(_loc_28.slice(1));
			}
			if (_loc_13 != "")
			{
				_loc_29 = _loc_13.split(",");
				if (_loc_29.length == 8)
				{
					_loc_29 = ["normal"].concat(_loc_29);
				}
				if (_loc_29.length == 9)
				{
					_loc_30 = _loc_29.shift();
					_loc_29[0] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[0]))));
					_loc_29[1] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[1]))));
					_loc_29[2] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[2]))));
					_loc_29[3] = Math.max(0, Math.min(1, Number(_loc_29[3])));
					_loc_29[4] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[4]))));
					_loc_29[5] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[5]))));
					_loc_29[6] = Math.round(Math.max(0, Math.min(255, Number(_loc_29[6]))));
					_loc_29[7] = Math.max(0, Math.min(1, Number(_loc_29[7])));
					if (!isNaN(_loc_29[0]) && !isNaN(_loc_29[1]) && !isNaN(_loc_29[2]) && !isNaN(_loc_29[3]))
					{
						g.characterControl.setEyebrowFill({r:_loc_29[0], g:_loc_29[1], b:_loc_29[2], a:_loc_29[3]});
					}
					if (!isNaN(_loc_29[4]) && !isNaN(_loc_29[5]) && !isNaN(_loc_29[6]) && !isNaN(_loc_29[7]))
					{
						g.characterControl.setEyebrowLine({r:_loc_29[4], g:_loc_29[5], b:_loc_29[6], a:_loc_29[7]});
					}
					g.characterControl.findEyebrows(_loc_30);
				}
			}
			if (_loc_11 != "")
			{
				_loc_31 = g.customElementLoader.tryToLoadBGFile(_loc_11);
			}
			else if (_loc_12 != "")
			{
				g.customElementLoader.clearCustomBackground();
				_loc_32 = uint(_loc_12);
				_loc_32 = Math.min(g.bgNum, _loc_32);
				g.bg.gotoAndStop(_loc_32);
				g.bgID = _loc_32;
				g.inGameMenu.setBGCheckboxes();
			}
			g.inGameMenu.updateHairList();
			g.inGameMenu.updateBreastSlider();
			g.inGameMenu.updateIrisList();
			g.inGameMenu.updateSkinList();
			g.inGameMenu.updateNoseList();
			g.inGameMenu.updateEarList();
			g.inGameMenu.updateEyebrowsList();
			g.inGameMenu.updateCharMenu(0, true);
			g.saveData.saveCharData();
			return;
		}
	}

}