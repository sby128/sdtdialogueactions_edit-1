package  
{
	import flash.events.Event;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.geom.Point;
	/**
	 * ...
	 * @author Pimgd
	 */
	public class HerTriggers extends CustomTriggerHolder
	{
		private const defaultPussydripFrameCount:uint = 50;
		private var masturbating:Boolean = false;
		private var volume:SoundTransform = new SoundTransform();
		private var squirting:Boolean = false;
		private var startingSquirt:Boolean = true;
		private var herOrgasm:Boolean = false;
		private var herPleasure:Number = 0;
		private var masState:Number = 0;
		private var sTime:Number = 0;
		private var theta:Number = 0;
		private var Droplet:Class;
		private var paletteUtils:Class;
		private var CharacterControl:Class;
		public function HerTriggers(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("WAKE_UP", 0, new FunctionObject(wakeUp, this, []));
			t.registerTrigger("KNOCK_OUT", 0, new FunctionObject(knockOut, this, []));
			t.registerTrigger("DEEPTHROAT", 0, new FunctionObject(deepthroat, this, []));
			t.registerTrigger("MASTURBATE_ON", 0, new FunctionObject(masturbateOn, this, []));
			t.registerTrigger("MASTURBATE_OFF", 0, new FunctionObject(masturbateOff, this, []));
			t.registerTrigger("MASTURBATE_STOP", 0, new FunctionObject(masturbateStop, this, []));
			t.registerTrigger("RANDOMIZE_HER", 0, new FunctionObject(randomizeHer, this, []));
			t.registerTrigger("RANDOMIZE_HER_BODY", 0, new FunctionObject(randomizeHerBody, this, []));
			t.registerTrigger("RESET_RESIST", 0, new FunctionObject(resetResist, this, []));
			t.registerTrigger("PUSSYDRIP", 0, new FunctionObject(startSquirtingZeroArgs, this, []));
			t.registerTrigger("PUSSYDRIP", 1, new FunctionObject(startSquirting, this, []));
			Droplet = m.getLoaderRef().eDOM.getDefinition("obj.Droplet") as Class;
			paletteUtils = m.getLoaderRef().eDOM.getDefinition("obj.graphics.PaletteUtils") as Class;
			CharacterControl = m.getLoaderRef().eDOM.getDefinition("obj.CharacterControl") as Class;
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableRead("da.canSpeakLineTrigger", new FunctionObject(canSpeakLineTrigger, this, []));
			v.registerVariableRead("da.her.clenchingTeeth", new FunctionObject(isClenchingTeeth, this, []));
			v.registerVariableRead("da.her.body.penis.minSize", new FunctionObject(getHerPenisMinSize, this, []));
			v.registerVariableRead("da.her.body.penis.maxSize", new FunctionObject(getHerPenisMaxSize, this, []));
			
			v.registerVariableGetterSetter("da.masturbation.herpleasure", new FunctionObject(getHerPleasure, this, []), new FunctionObject(setHerPleasure, this, []));
			v.registerVariableGetterSetter("da.pleasurePercentage", new FunctionObject(getHimPleasure, this, []), new FunctionObject(setHimPleasure, this, []));
			v.registerVariableGetterSetter("da.breathPercentage", new FunctionObject(getHerBreathPercentage, this, []), new FunctionObject(setHerBreathPercentage, this, []));
			v.registerVariableGetterSetter("da.oxygenPercentage", new FunctionObject(getHerOxygenPercentage, this, []), new FunctionObject(setHerOxygenPercentage, this, []));
			v.registerVariableGetterSetter("da.her.body.tan", new FunctionObject(getHerTanType, this, []), new FunctionObject(setHerTanType, this, []));
			v.registerVariableGetterSetter("da.her.body.tan.amount", new FunctionObject(getHerTan, this, []), new FunctionObject(setHerTan, this, []));
			v.registerVariableGetterSetter("da.her.body.breasts", new FunctionObject(getHerBreastSize, this, []), new FunctionObject(setHerBreastSize, this, []));
			v.registerVariableGetterSetter("da.her.body.scale", new FunctionObject(getHerBodyScale, this, []), new FunctionObject(setHerBodyScale, this, []));
			v.registerVariableGetterSetter("da.her.body.hair", new FunctionObject(getHerHairType, this, []), new FunctionObject(setHerHairType, this, []));
			v.registerVariableGetterSetter("da.her.body.skin", new FunctionObject(getHerSkinType, this, []), new FunctionObject(setHerSkinType, this, []));
			v.registerVariableGetterSetter("da.her.body.ears", new FunctionObject(getHerEarType, this, []), new FunctionObject(setHerEarType, this, []));
			v.registerVariableGetterSetter("da.her.body.nose", new FunctionObject(getHerNoseType, this, []), new FunctionObject(setHerNoseType, this, []));
			v.registerVariableGetterSetter("da.her.body.iris", new FunctionObject(getHerIrisType, this, []), new FunctionObject(setHerIrisType, this, []));
			v.registerVariableGetterSetter("da.her.body.eyebrow", new FunctionObject(getHerEyebrowType, this, []), new FunctionObject(setHerEyebrowType, this, []));
			v.registerVariableGetterSetter("da.her.body.mascara", new FunctionObject(getHerMascaraPercentage, this, []), new FunctionObject(setHerMascaraPercentage, this, []));
			v.registerVariableGetterSetter("da.her.body.freckles", new FunctionObject(getHerFrecklePercentage, this, []), new FunctionObject(setHerFrecklePercentage, this, []));
			v.registerVariableGetterSetter("da.her.body.penis", new FunctionObject(getHerPenisType, this, []), new FunctionObject(setHerPenisType, this, []));
			v.registerVariableGetterSetter("da.her.body.penis.length", new FunctionObject(getHerPenisLength, this, []), new FunctionObject(setHerPenisLength, this, []));
			v.registerVariableGetterSetter("da.her.body.penis.width", new FunctionObject(getHerPenisWidth, this, []), new FunctionObject(setHerPenisWidth, this, []));
			
			var rgba:Array = ["r", "g", "b", "a"];
			var rgbaVariables:Array = [
										["da.her.body.iris.", "irisRGB", "setIrisFill"],
										["da.her.body.sclera.", "scleraRGB", "setSclera"],
										["da.her.body.eyeshadow.", "eyeShadowRGB", "setEyeShadow"],
										["da.her.body.lipstick.", "lipstickRGB", "setLipstick"],
										["da.her.body.eyebrow.outline.", "eyebrowLineRGB", "setEyebrowLine"],
										["da.her.body.eyebrow.fill.", "eyebrowFillRGB", "setEyebrowFill"],
										["da.her.body.blush.", "blushRGB", "setBlush"],
										["da.her.body.mascara.", "mascaraRGB", "setMascara"],
										["da.her.body.freckles.", "frecklesRGB", "setFreckles"],
										["da.her.body.nailpolish.", "nailPolishRGB", "setNailPolish"]
									];
			for each (var arr:Array in rgbaVariables) {
				for each (var colorSegment:String in rgba) {
					v.registerVariableGetterSetter(arr[0] + colorSegment, new FunctionObject(getRGBAColorSegment, this, [arr[1], colorSegment]), new FunctionObject(setRGBAColorSegment, this, [arr[1], arr[2], arr[0] + colorSegment, colorSegment]));
				}
			}
			
			var hslc:Array = ["h", "s", "l", "c"];
			var hslcVariables:Array = [
				["da.her.body.hair.", "hairHSL", "applyHairHSL"],
				["da.her.body.skin.", "skinHSL", "applySkinHSL"],
				["da.him.body.skin.", "hisSkinHSL", "applyHisSkinHSL"]
			];
			for each (var arr:Array in hslcVariables) {
				for each (var colorSegment:String in hslc) {
					v.registerVariableGetterSetter(arr[0] + colorSegment, new FunctionObject(getHSLCColorSegment, this, [arr[1], colorSegment]), new FunctionObject(setHSLCColorSegment, this, [arr[1], arr[2], arr[0] + colorSegment, colorSegment]));
				}
			}
		}
		
		public function isClenchingTeeth(...args):Boolean {
			return g.her.teethClenched;
		}
		
		public function getRGBAColorSegment(rgbVariableName:String, colorSegment:String, ...args):Number {
			return g.characterControl[rgbVariableName][colorSegment];
		}
		
		public function getRGBAColorObject(rgbVariableName: String):Object {
			var original:Object = g.characterControl[rgbVariableName];
			var copy: Object = { 
				r: original.r, 
				g: original.g, 
				b: original.b
			};
			if (original.a !== undefined) {
				copy.a = original.a;
			}
			return copy;
		}
		
		public function setRGBAColorSegment(rgbVariableName:String, setterName:String, daVariableName:String, colorSegment:String, value:*):void {
			var newVal:Number = calculateNewRGBAValue(new FunctionObject(getRGBAColorSegment, this, [rgbVariableName, colorSegment]), value, colorSegment);
			if (isNaN(newVal)) {
				m.displayMessageRed(daVariableName + colorSegment + " attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			var newRGBColorObject:Object = getRGBAColorObject(rgbVariableName);
			newRGBColorObject[colorSegment] = newVal;
			g.characterControl[setterName](newRGBColorObject);
		}
		
		public function getHSLCColorSegment(hslcVariableName:String, colorSegment:String, ...args):Number {
			return g.characterControl[hslcVariableName][colorSegment];
		}
		
		public function getHSLCColorObject(hslcVariableName: String):Object {
			var original:Object = g.characterControl[hslcVariableName];
			return { 
				h: original.h, 
				s: original.s, 
				l: original.l, 
				c: original.c 
			};
		}
		
		public function setHSLCColorSegment(hslcVariableName:String, setterName:String, daVariableName:String, colorSegment:String, value:*):void {
			var newVal:Number = calculateNewHSLCValue(new FunctionObject(getHSLCColorSegment, this, [hslcVariableName, colorSegment]), value, colorSegment);
			if (isNaN(newVal)) {
				m.displayMessageRed(daVariableName + colorSegment + " attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			var newHslcObject:Object = getHSLCColorObject(hslcVariableName);
			newHslcObject[colorSegment] = newVal;
			g.characterControl[setterName](newHslcObject);
		}
		
		public function getHerTan(...args):Number {
			return g.her.tan._currentTanAlpha;
		}
		
		public function setHerTan(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerTan, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.tan.amount attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 1);
			newVal = Math.max(newVal, 0);
			g.her.tan.setTanAlpha(newVal);
			g.her.tan.updateAlphaSlider();
		}
		
		public function getHerTanType(...args):String {
			return g.her.tan._tanNameList[g.her.tan._currentTanID];
		}
		
		public function setHerTanType(value:*):void {
			var newVal:int = g.her.tan._tanNameList.indexOf(value);
			if (newVal == -1) {
				m.displayMessageRed("da.her.body.tan attempted set to " + value + ", unable to find tantype");
			} else {
				g.her.tan.setTan(newVal);
			}
		}
		
		public function getHerPenisType(...args):String {
			return g.her._penisControl.penisNameList[g.her._penisControl.currentPenisID];
		}
		
		public function setHerPenisType(value:*):void {
			var penisIndex:int = g.her._penisControl.penisNameList.indexOf(value);
			if (penisIndex == -1) {
				m.displayMessageRed("da.her.body.penis attempted set to " + value + ", unable to find penistype");
				return;
			}
			g.her._penisControl.penisControl.select(penisIndex);
		}
		
		public function getHerPenisMinSize(...args):Number {
			return g.her._penisControl.MIN_PENIS_SIZE;
		}
		
		public function getHerPenisMaxSize(...args):Number {
			return g.her._penisControl.MAX_PENIS_SIZE;
		}
		
		public function getHerPenisWidth(...args):Number {
			return g.her._penisControl.penis.scaleY;
		}
		
		public function setHerPenisWidth(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerPenisWidth, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.penis.width attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			g.her._penisControl.loadPenisScales(g.her._penisControl.currentPenisLengthScale, newVal);
		}
		
		public function getHerPenisLength(...args):Number {
			return g.her._penisControl.penis.scaleX;
		}
		
		public function setHerPenisLength(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerPenisLength, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.penis.length attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			g.her._penisControl.loadPenisScales(newVal, g.her._penisControl.currentPenisWidthScale);
		}
		
		public function getHerHairType(...args):String {
			var charCodeString:String = g.inGameMenu.getSaveDataString();
			var charCodeArray:Array = charCodeString.split(";");
			var charCodeSectionArray:Array = new Array();
			for each (var charCodeSection:String in charCodeArray) {
				var charCodeSectionArray:Array = charCodeSection.split(":");
				if (charCodeSectionArray[0] == "hair") {
					return charCodeSectionArray[1].split(",")[0];
				}
			}
			return g.inGameMenu.charNames[g.characterControl.currentHair];
		}
		
		public function setHerHairType(value:*):void {
			g.characterControl.findHair(value);
			g.inGameMenu.updateHairList();
		}
		
		public function getHerEyebrowType(...args):String {
			return CharacterControl.eyebrowsTypeList[g.characterControl.currentEyebrows];
		}
		
		public function setHerEyebrowType(value:*):void {
			g.characterControl.findEyebrows(value);
			g.inGameMenu.updateEyebrowsList();
		}
		
		public function getHerMascaraPercentage(...args):Number {
			return g.mascaraAmount;
		}
		
		public function setHerMascaraPercentage(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerMascaraPercentage, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.mascara attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 100);
			newVal = Math.max(newVal, 0);
			g.mascaraAmount = newVal;
			g.inGameMenu.updateMascaraSlider();
		}
		
		public function getHerFrecklePercentage(...args):Number {
			return g.frecklesAmount;
		}
		
		public function setHerFrecklePercentage(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerFrecklePercentage, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.freckles attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 100);
			newVal = Math.max(newVal, 0);
			g.characterControl.setFreckleAmount(newVal);
			g.inGameMenu.updateFrecklesSlider();
		}
		
		public function getHerIrisType(...args):String {
			return g.inGameMenu.irisTypes[g.characterControl.currentIris];
		}
		
		public function setHerIrisType(value:*):void {
			g.characterControl.findIris(value);
			g.inGameMenu.updateIrisList();
		}
		
		public function getHerNoseType(...args):String {
			return CharacterControl.noseTypeList[g.characterControl.currentNose];
		}
		
		public function setHerNoseType(value:*):void {
			g.characterControl.findNose(value);
			g.inGameMenu.updateNoseList();
		}
		
		public function getHerEarType(...args):String {
			return CharacterControl.earTypeList[g.characterControl.currentEar];
		}
		
		public function setHerEarType(value:*):void {
			g.characterControl.findEar(value);
			g.inGameMenu.updateEarList();
		}
		
		public function getHerSkinType(...args):String {
			return g.inGameMenu.skinTypes[g.characterControl.currentSkin];
		}
		
		public function setHerSkinType(value:*):void {
			g.characterControl.findSkin(value);
			g.inGameMenu.updateSkinList();
		}
		
		public function getHerBodyScale(...args):Number {
			return g.her.bodySize;
		}
		
		public function setHerBodyScale(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerBodyScale, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.scale attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.max(newVal, 0);
			g.her.setBodyScaleFromSlider(newVal);
			g.inGameMenu.updateBodySlider();
		}
		
		public function getHerBreastSize(...args):Number {
			return g.characterControl.breastSize;
		}
		
		public function setHerBreastSize(value:*):void {
			var newVal:Number = calculateNewValue(new FunctionObject(getHerBreastSize, this, []), value);
			if (isNaN(newVal)) {
				m.displayMessageRed("da.her.body.breasts attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 149);
		    newVal = Math.max(newVal, 0);
			g.characterControl.setBreasts(newVal);
			g.strandControl.checkElementAnchors(g.her.torso.midLayer.rightBreast); //This is included in the "breastSliderRelease" handler...
			g.inGameMenu.updateBreastSlider();
		}
		
		private function calculateNewRGBAValue(getter:FunctionObject, newValue:*, colorSegment:String):Number {
			var newVal:Number = calculateNewValue(getter, newValue);
			if (isNaN(newVal)) {
				return NaN;
			}
			var maxLimit:Number = 255;
			if (colorSegment == "a") {
				maxLimit = 1;
			}
			newVal = Math.max(newVal, 0);
			newVal = Math.min(newVal, maxLimit);
			return newVal;
		}
		
		private function calculateNewHSLCValue(getter:FunctionObject, newValue:*, colorSegment:String):Number {
			var newVal:Number = calculateNewValue(getter, newValue);
			if (isNaN(newVal)) {
				return NaN;
			}
			var maxLimit:Number = 4;
			var minLimit:Number = 0;
			if (colorSegment == "h") {
				maxLimit = 180;
				minLimit = -180;
			}
			newVal = Math.max(newVal, minLimit);
			newVal = Math.min(newVal, maxLimit);
			return newVal;
		}
		
		private function calculateNewValue(getter:FunctionObject, newValue:*):Number {
			var newVal:Number = 0;
			if (newValue is Number) {
				newVal = newValue;
			} else if (newValue is String && !isNaN(Number(newValue))) {
				if ((newValue as String).charAt(0) == "-" || (newValue as String).charAt(0) == "+" ) {
					newVal = getter.call() + Number(newValue);
				} else {
					newVal = Number(newValue);
				}
			} else {
				return NaN;
			}
			return newVal;
		}
		
		public function canSpeakLineTrigger(...args):Number {
			if (!g.her.gagged && !g.her.mouthFull && !g.her.passedOut && !g.her.swallowing) {
				return 1;
			} else {
				return 0;
			}
		}
		
		public function getHerBreathPercentage(...args):Number {
			return 100 - ((g.currentBreathLevel / g.breathLevelMax) * 100);
		}
		
		public function setHerBreathPercentage(value:*):void {
			var newVal:Number = 0;
			if (value is Number) {
				newVal = 100 - value;
			} else if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = getHerBreathPercentage();
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal - Number(value);//It's inverted. Goes from 0 to max instead from max to 0.
				} else {
					newVal = Number(value);
				}
			} else {
				m.displayMessageRed("da.breathPercentage attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 100);
			newVal = Math.max(newVal, 0);
			g.currentBreathLevel = (g.breathLevelMax / 100) * newVal;
		}
		
		public function getHerOxygenPercentage(...args):Number {
			return 100 - ((g.her.passOutFactor / g.her.passOutMax) * 100);
		}
		
		public function setHerOxygenPercentage(value:*):void {
			var newVal:Number = 0;
			if (value is Number) {
				newVal = 100 - value;
			} else if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = getHerOxygenPercentage();
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal - Number(value);//It's inverted.
				} else {
					newVal = Number(value);
				}
			} else {
				m.displayMessageRed("da.oxygenPercentage attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 100);
			newVal = Math.max(newVal, 0);
			g.her.passOutFactor = (g.her.passOutMax / 100) * newVal;
		}
		
		public function getHimPleasure(...args):Number {
			var _loc2_:* = int(g.him.pleasure / (g.him.ejacPleasure / 0.97) * 10000) / 100;
			if (_loc2_ >= 97) {
				_loc2_ = _loc2_ + g.him.maxPleasureTimer / 20;
			}
			return Math.min(100, _loc2_);
		}
		
		public function setHimPleasure(value:*):void {
			var newVal:Number = 0;
			if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = getHimPleasure();
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal + Number(value);
				} else {
					newVal = Number(value);
				}
			} else if (value is Number) {
				newVal = value;
			} else {
				m.displayMessageRed("da.pleasurePercentage attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 100);
			newVal = Math.max(newVal, 0);
			setHimPleasureInternal(newVal);
		}
		
		private function setHimPleasureInternal(value:Number):void {
			g.him.pleasure = (g.him.ejacPleasure / 97) * value;
		}
		
		public function getHerPleasure(...args):Number {
			return (herPleasure * 100);
		}
		
		public function setHerPleasure(value:*):void {
			var newVal:Number = 0;
			if (value is String && !isNaN(Number(value))) {
				var currentVal:Number = herPleasure;
				if ((value as String).charAt(0) == "-" || (value as String).charAt(0) == "+" ) {
					newVal = currentVal + (Number(value)/100);
				} else {
					newVal = (Number(value)/100);
				}
			} else if (value is Number) {
				newVal = value/100;
			} else {
				m.displayMessageRed("da.masturbation.herpleasure attempted set to " + value + ", unable to identify value as numeric");
				return;
			}
			newVal = Math.min(newVal, 1);
			newVal = Math.max(newVal, 0);
			herPleasure = newVal;
		}
		
		public function wakeUp(...args):void {
			g.her.wakeUp();
		}
		
		public function knockOut(...args):void {
			g.her.passOut();
		}
		public function deepthroat(...args):void {
			g.her.maxIntroDist = 150;
		}
		
		public function randomizeHer(...args):void {
			g.inGameMenu.shuffle();
		}
		
		public function randomizeHerBody(...args):void {
			var _loc1_:Array = new Array();
			var _loc2_:uint = Math.floor(Math.random() * 4) + 1;
			var _loc3_:uint = 0;
			var PaletteUtils = paletteUtils;
			while(_loc3_ < _loc2_)
			{
			   _loc1_[_loc3_] = PaletteUtils.getHue(Math.random() * 360,PaletteUtils.midRandom(),Math.random() > 0.6?PaletteUtils.midRandom():null);
			   _loc3_++;
			}
			var _loc4_:Array = new Array();
			_loc3_ = 0;
			while(_loc3_ < 2)
			{
			   _loc4_[_loc3_] = PaletteUtils.getHue(Math.random() * 190 + 170,Math.random(),Math.random() > 0.4?Math.random() * 0.5:Math.random());
			   _loc3_++;
			}
			var _loc5_:uint = Math.floor(Math.random() * g.inGameMenu.charNames.length);
			g.characterControl.setHair(_loc5_);
			g.characterControl.randomCurrentElements();
			g.inGameMenu.updateHairList();
			var _loc6_:uint = Math.floor(Math.random() * 150);
			g.characterControl.setBreasts(_loc6_);
			g.inGameMenu.updateBreastSlider();
			var _loc8_:uint = Math.floor(Math.random() * g.inGameMenu.irisTypes.length);
			g.characterControl.setIris(_loc8_);
			g.inGameMenu.updateIrisList();
			g.characterControl.setIrisFill(PaletteUtils.getHue(Math.random(),Math.random() * 0.8,PaletteUtils.midRandom() * 0.5));
			var _loc9_:uint = Math.floor(Math.random() * g.inGameMenu.skinTypes.length);
			g.characterControl.setSkin(_loc9_);
			g.inGameMenu.updateSkinList();
			g.her.setBodyScaleFromSlider(Math.random());
			g.inGameMenu.customMenu.bodySlider.setPos(g.her.bodySize);
			g.characterControl.setLipstick(PaletteUtils.randomSwatch(_loc4_));
			g.characterControl.setEyeShadow(PaletteUtils.randomSwatch(_loc4_));
			g.characterControl.setNailPolish(PaletteUtils.randomSwatch(_loc4_));
			
			g.saveData.saveCharData();
		}
		
		public function resetResist(...args):void {
			g.her.setIntroResistance(g.inGameMenu.costumeMenu.resistanceSlider.currentValue(100));
			g.her.setThroatResistance(g.inGameMenu.costumeMenu.throatResistanceSlider.currentValue(100));
			g.her.maxIntroDist = g.her.introStartDist;
			g.her.firstDT = true;
			g.her.intro = true;
			g.him.deepestPos = 0;
		}
		
		public function masturbateOn(...args):void {
			if (!masturbating) {
				g.her.setLeftArmPosition(1);
				masturbating = true;
				m.mc.addEventListener(Event.ENTER_FRAME, masturbate);
			}
		}
		
		public function masturbateOff(...args):void {
			if (masturbating) {
				m.mc.removeEventListener(Event.ENTER_FRAME, masturbate);
				masturbating = false;
				m.mc.addEventListener(Event.ENTER_FRAME, fixMas);
			}
		}
		public function masturbate(e:Event = null):void {
			if (g.gamePaused || !g.gameRunning) {
				return;
			}
			tickCode();
			m.mc.removeEventListener(Event.ENTER_FRAME, masturbateCooldown);
			if (g.her.getChildAt(1) != g.her.leftArmContainer) {
				g.her.addChildAt(g.her.leftArmContainer, 1);
			}
			g.her.leftArmIK.newTarget(new Point(-25 + Math.cos(theta) * 2, 240 + Math.sin(theta) * 12), g.her.torso.back, true);
			g.her.leftArmIK.setEndRotationTarget(45, [g.her.torso.back, g.her.torso]);
			herPleasure += 0.0005 + g.her.vigour / 1000000;
			if (masState == 0 && !squirting) {
				theta += Math.max(herPleasure, 0.2) + g.her.vigour / 3000;
				theta %= 360;
				if (herPleasure >= 1) {
					herPleasure = 0;
					masState = 1;
				}
			} else if (masState == 1) {
				herOrgasm = true;
				startSquirtingZeroArgs();
				masState = 0;
			}
		}
		
		public function startSquirtingZeroArgs(...args):void {
			startSquirting(defaultPussydripFrameCount);
		}
		
		public function startSquirting(duration:Number):void {
			if (isNaN(duration)) {
				return;
			}
			g.her.closeEye();
			if (!squirting) {
				m.mc.addEventListener(Event.ENTER_FRAME, tickCode);
				squirting = true;
			}
			sTime += duration;
			if (!g.her.speaking && !g.her.mouthFull && !g.her.passedOut && !g.her.swallowing && herOrgasm) {
				playSound(g.soundControl.moan2.play(), 0.5);
			}
		}
		
		public function stopSquirting():void {
			g.her.openEye();
			squirting = false;
			m.mc.removeEventListener(Event.ENTER_FRAME, tickCode);
			if (herOrgasm) {
				g.her.setSpeaking(false);
				m.getLineTypeManager().triggerLine("orgasm", 130);
				herOrgasm = false;
			}
		}
		
		public function tickCode(e:Event = null):void {
			if (g.gamePaused || !g.gameRunning) {
				return;
			}
			if (sTime > 0) {
				sTime--;
				var newDroplet:* = new Droplet(g.sceneLayer.globalToLocal(g.her.torso.backside.localToGlobal(new Point(30, 240))), new Point(Math.random() * 18 - 5, Math.random() * 25), Math.random() + 0.1, 0.01);
				newDroplet.padding = 2000;
				g.strandBackLayer.addChild(newDroplet);
			} else {
				stopSquirting();
			}
		}
		
		public function fixMas(e):void {
			if (g.gamePaused || !g.gameRunning) {
				return;
			}
			tickCode();
			if (!masturbating) {
				m.mc.removeEventListener(Event.ENTER_FRAME, masturbateCooldown);
				if (g.her.currentLeftArmPosition == 3) {
					g.her.pleasure = 0;
				}
				if (herPleasure > 0.2) {
					herPleasure -= 0.0005;
					g.her.leftArmIK.newTarget(new Point(-25 + Math.cos(theta) * 2, 240 + Math.sin(theta) * 12), g.her.torso.back, true);
					theta -= Math.max(herPleasure, 0.2) + g.her.vigour / 3000;
					theta = Math.max(theta + 360, 360);
				} else {
					herPleasure = 0;
					masState = 0;
					masturbateStop();
					m.mc.removeEventListener(Event.ENTER_FRAME, masturbateCooldown);
					m.mc.removeEventListener(Event.ENTER_FRAME,fixMas);
				}
			} else {
				m.mc.removeEventListener(Event.ENTER_FRAME,fixMas);
			}
		}
		
		public function masturbateStop(...args):void {
			masturbateOff();
			//g.her.setRightArmPosition(g.her.currentRightArmPosition); //she uses her left arm, only need to set left
			g.her.setLeftArmPosition(g.her.currentLeftArmPosition);
			g.her.updateArms();
			m.mc.removeEventListener(Event.ENTER_FRAME, fixMas);
			m.mc.addEventListener(Event.ENTER_FRAME, masturbateCooldown);
		}
		
		public function masturbateCooldown(...args):void {
			if (g.gamePaused || !g.gameRunning) {
				return;
			}
			herPleasure = Math.max(herPleasure - 0.0005, 0);
			if (herPleasure == 0) {
				m.mc.removeEventListener(Event.ENTER_FRAME, masturbateCooldown);
			}
		}
		
		private function playSound(sound:SoundChannel, value:Number):void {
			volume.volume = value;
			sound.soundTransform = volume;
		}
	}

}