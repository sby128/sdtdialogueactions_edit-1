package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class Trigger 
	{
		private var argumentCount:uint;
		private var triggerName:String;
		public function Trigger(name:String, argCount:uint) 
		{
			triggerName = name;
			argumentCount = argCount;
		}
		
		public function getName():String {
			return triggerName;
		}
		
		public function getArgumentCount():uint {
			return argumentCount;
		}
		
	}

}