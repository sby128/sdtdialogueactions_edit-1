package  
{
	/**
	 * ...
	 * @author Pimgd
	 */
	public class SDTVariables extends CustomVariableHolder
	{
		
		public function SDTVariables(G:Object, M:Main) 
		{
			super(G, M);
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableRead("da.mouthfull", new FunctionObject(getMouthFull, this, []));
			v.registerVariableRead("da.held", new FunctionObject(getHeld, this, []));
			v.registerVariableRead("da.finishes", new FunctionObject(getFinishes, this, []));
			v.registerVariableRead("da.her.name", new FunctionObject(getHerName, this, []));
			v.registerVariableRead("da.her.namePossessive", new FunctionObject(getHerPossessiveName, this, []));
			v.registerVariableRead("da.him.name", new FunctionObject(getHisName, this, []));
			v.registerVariableRead("da.him.namePossessive", new FunctionObject(getHisPossessiveName, this, []));
			
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("hoverOptions"), v); //For certain positions, this will be useful
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("showMouse"), v); //For extra immersion
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("strandShaders"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("mirrored"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("spit"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("tears"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("mascara"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("smudging"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("nostrilSpray"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("sweat"), v);
			//registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("introSound"), v); //this stays turned off, kthx - who wants to hear doo do do doooooo everytime they're testing
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("breathing"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("gagging"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("coughing"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("tongue"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("dialogue"), v);//Uhm... ... Yeah... Don't turn this off, it will cause DA to be useless
			//I'm leaving the ability to turn dialogue off as the ultimate "Game Over" for a bundle - "I'll NEVER talk to you again!" that sort of stuff.
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("invertControls"), v);
			registerHandlerForBooleanSDTFeature(createHandlerForBooleanSDTFeature("bukkakeMode"), v);//hey, a secret switch!
		}
		
		private function buildFunctionName(name:String):String {
			return "cb" + name.charAt(0).toUpperCase() + name.substr(1) + "Clicked";
		}
		
		private function createHandlerForBooleanSDTFeature(name:String):SDTBooleanFeatureHandler {
			return new SDTBooleanFeatureHandler(m, "da.sdtoptions." + name, g, name, new FunctionObject(g.inGameMenu[buildFunctionName(name)], g.inGameMenu, [null]));
		}
		
		private function registerHandlerForBooleanSDTFeature(handler:SDTBooleanFeatureHandler, v:VariableManager):void {
			v.registerVariableRead(handler.getVariableName(), new FunctionObject(handler.isEnabledAsUint, handler, []));
			v.registerVariableWrite(handler.getVariableName(), new FunctionObject(handler.setEnabledness, handler, [])); 
		}
		
		public function getMouthFull():Boolean {
			return g.her.mouthFull;
		}
		
		public function getHeld():Boolean {
			return g.him.currentArmPosition == 0;
		}
		
		public function getFinishes():int {
			return g.totalFinishes;
		}
		
		public function getHerName():String {
			return g.dialogueControl.herName();
		}
		
		public function getHerPossessiveName():String {
			return g.dialogueControl.herPosessive();
		}
		
		public function getHisName():String {
			return g.dialogueControl.partnerName();
		}
		
		public function getHisPossessiveName():String {
			return g.dialogueControl.partnerPosessive();
		}
	}

}