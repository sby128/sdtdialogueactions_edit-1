package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_Ow01.swf",symbol="WWSound_Ow01")]
	
	public dynamic class WWSound_Ow01 extends Sound {
		
		public function WWSound_Ow01() {
			super();
		}
	}

}
