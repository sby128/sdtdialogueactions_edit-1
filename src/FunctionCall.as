package 
{
	
	/**
	 * ...
	 * @author Pimgd
	 */
	public class FunctionCall
	{
		public var listenersRemaining:Array;
		public var SDTFunction:FunctionObject;
		public var SDTCalled:Boolean;
		public var hooked:Boolean;
		public function FunctionCall(listeners:Array, sdtFunction:FunctionObject, shouldCallSDT:Boolean) {
			listenersRemaining = listeners.concat();
			SDTFunction = sdtFunction;
			SDTCalled = false;
			hooked = shouldCallSDT;
		}
		public function callFunction(... args) {
			while (listenersRemaining.length != 0) {
				var listener = listenersRemaining.pop();
				var ret = listener.apply(null, args);
				if (ret != undefined) return ret;
			}
			if (hooked && !SDTCalled) {
				SDTCalled = true;
				return SDTFunction.apply(args);
			}
		}
		public function superCall(... args) {
			return callFunction(args);
		}
	}
	
}