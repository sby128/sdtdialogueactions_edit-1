﻿package {
	import flash.events.Event;
	
	//TODO add support for dollar prefix... not sure if possible since Loader handles the path
	
	public class ModActions extends CustomTriggerHolder {
		
		public function ModActions(G:Object, M:Main) {
			super(G, M);
		}
		
		private var modToLoad:String;
		
		private var hasLineListener:Boolean = false;
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("LOAD_MOD", 0, new FunctionObject(this.triggerLoadMod, this, []));
			t.registerTrigger("CLEAR_MOD", 0, new FunctionObject(this.clearMod, this, []));
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableWrite("da.mod.load", new FunctionObject(setModToLoad, this, []));
		}
		
		public function triggerLoadMod(... args):void {
			if (modToLoad != "") {
				m.loadData(modToLoad);
			} else if (!hasLineListener) {
				m.displayMessageRed("No mod set for loading. (Error: [LOAD_MOD] without da.mod.load set)");
			}
		}
		
		public function clearMod(... args):void {
			g.customElementLoader.clearInstancedMods();
			g.soundControl.resetAudioMods();
		}
		
		public function setModToLoad(mod:String):void {
			modToLoad = mod;
			m.addLineChangeListener(new FunctionObject(onLineChange, this, []));
			hasLineListener = true;
		}
		
		public function onLineChange(... args):void {
			if ((g.dialogueControl.sayingPhrase.indexOf("[LOAD_MOD]") == -1)) {
				m.loadData(modToLoad);
			}
			hasLineListener = false;
		}
	}

}