package {
	import flash.events.Event;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.media.Sound;
	import flash.net.URLRequest;
	import flash.events.IOErrorEvent;
	
	/**
	 * ...
	 * @author Pimgd
	 */
	
	public class SoundTriggers extends CustomTriggerHolder {
		private var loadedClasses:Boolean = false;
		private var volume:SoundTransform = new SoundTransform();
		private var gagFunctionObject:FunctionObject;
		private var bgmChannel:SoundChannel = null;
		private var bgmTransform:SoundTransform = new SoundTransform();
		private var sfxChannel:SoundChannel = null;
		private var sfxTransform:SoundTransform = new SoundTransform();
		private var gagging:Boolean = false;
		
		// Added by WeeWillie 9/8/14
		private var voiceID:int = 1;
		
		public function SoundTriggers(G:Object, M:Main) {
			super(G, M);
		}
		
		public function registerTriggers(t:TriggerManager):void {
			t.registerTrigger("GAG", 0, new FunctionObject(playGagSound, this, []));
			t.registerTrigger("MOAN", 0, new FunctionObject(playMoanSound, this, []));
			
			// Added by WeeWillie 2/13/14
			t.registerTrigger("SLAP", 0, new FunctionObject(playSlapSound, this, []));
			t.registerTrigger("OW", 0, new FunctionObject(playOwSound, this, []));
			t.registerTrigger("AH_HARD", 0, new FunctionObject(playAhHardSound, this, []));
			t.registerTrigger("OH_HARD", 0, new FunctionObject(playOhHardSound, this, []));
			t.registerTrigger("OH_SOFT", 0, new FunctionObject(playOhSoftSound, this, []));
			
			// Added by WeeWillie 9/8/14
			t.registerTrigger("SETVOICE", 1, new FunctionObject(setVoice, this, []));
			
			t.registerTrigger("PLAY_BGM", 1, new FunctionObject(playBGM, this, []));
			t.registerTrigger("STOP_BGM", 0, new FunctionObject(stopBGM, this, []));
			
			t.registerTrigger("PLAY_SFX", 1, new FunctionObject(playSFX, this, []));
			t.registerTrigger("STOP_SFX", 0, new FunctionObject(stopSFX, this, []));
			
			initClasses();
		}
		
		public function registerVariables(v:VariableManager):void {
			v.registerVariableWrite("da.bgm.volume", new FunctionObject(setVolume, this, []));
		}
		
		public function checkInitClasses():void {
			if (!loadedClasses) {
				initClasses();
			}
		}
		
		public function initClasses():void {
			var l:* = m.getLoaderRef();
			var sfxWretch1:Class = l.eDOM.getDefinition("sfxWretch1") as Class;
			var sfxWretch2:Class = l.eDOM.getDefinition("sfxWretch2") as Class;
			var sfxWretch3:Class = l.eDOM.getDefinition("sfxWretch3") as Class;
			
			var sfxMoan1:Class = l.eDOM.getDefinition("sfxMoan1") as Class;
			var sfxMoan2:Class = l.eDOM.getDefinition("sfxMoan2") as Class;
			
			var sfxMoanSuck1:Class = l.eDOM.getDefinition("sfxMoanSuck1") as Class;
			var sfxMoanSuck2:Class = l.eDOM.getDefinition("sfxMoanSuck2") as Class;
			g.soundControl.wretch1 = new sfxWretch1();
			g.soundControl.wretch2 = new sfxWretch2();
			g.soundControl.wretch3 = new sfxWretch3();
			
			g.soundControl.moan1 = new sfxMoan1();
			g.soundControl.moan2 = new sfxMoan2();
			
			g.soundControl.moanSuck1 = new sfxMoanSuck1();
			g.soundControl.moanSuck2 = new sfxMoanSuck2();
			
			loadedClasses = true;
		}
		
		private function loadSound(filename:String, errorMessageOnFileNotFound:String):Sound 
		{
			var fullPath:String = m.getFileReferenceHandler().convertFilePath(filename);
			var listener:Function = function():void { m.displayMessageRed(errorMessageOnFileNotFound + fullPath) };
			
			var snd:Sound = new Sound();
			snd.addEventListener(IOErrorEvent.IO_ERROR, listener);
			snd.load(new URLRequest(fullPath), null);
			
			return snd;
		}
		
		public function playSFX(filename:String):void {
			checkInitClasses();
			stopSFX();
			
			var snd:Sound = loadSound(filename, "SFX not found: ");
			sfxChannel = snd.play(0, 0, null);
			sfxChannel.soundTransform = sfxTransform;
		}
		
		public function stopSFX(... args):void {
			if (sfxChannel != null) {
				sfxChannel.stop();
				sfxChannel = null;
			}
		}
		
		public function playBGM(filename:String):void {
			checkInitClasses();
			stopBGM();
			
			var snd:Sound = loadSound(filename, "BGM not found: ");
			bgmChannel = snd.play(0, 999999, null);
			bgmChannel.soundTransform = bgmTransform;
		}
		
		public function stopBGM(... args):void {
			if (bgmChannel != null) {
				bgmChannel.stop();
				bgmChannel = null;
			}
		}
		
		public function setVolume(value:*):void {
			bgmTransform.volume = value;
			if (bgmChannel != null) {
				bgmChannel.soundTransform = bgmTransform;
			}
		}
		
		public function playMoanSound(... args):void {
			checkInitClasses();
			var random:Number = Math.random();
			if (random <= 0.3) {
				playSound(g.soundControl.moan1.play(), 0.5);
			} else if (random <= 0.6) {
				playSound(g.soundControl.moanSuck1.play(), 0.5);
			} else {
				playSound(g.soundControl.moanSuck2.play(), 0.5);
			}
		}
		
		// Added by WeeWillie 9/8/14
		// Set which girl voice will run with the Oh, Ah, and Ow sound calls.
		public function setVoice(newVoiceID:String):void {
			var val:Number = 0;
			
			val = Number(newVoiceID);
			if (isNaN(val)) {
				m.displayMessageRed("Invalid Voice ID of " + newVoiceID);
			} else if (val != 1 && val != 2) {
				m.displayMessageRed("Voice ID of " + newVoiceID + " is out valid range.");
			} else {
				voiceID = val;
			}
		}
		
		// Added by WeeWillie 2/13/14
		public function playSlapSound(... args):void {
			var slap01:WWSound_Slap01 = new WWSound_Slap01();
			var soundChannel:SoundChannel = slap01.play();
		}
		
		public function playOwSound(... args):void {
			var random:Number = Math.random();
			var soundChannel:SoundChannel;
			if (voiceID == 1) {
				if (random <= 0.5) {
					var ow01:WWSound_Ow01 = new WWSound_Ow01();
					soundChannel = ow01.play();
				} else {
					var ow02:WWSound_Ow02 = new WWSound_Ow02();
					soundChannel = ow02.play();
				}
			} else {
				var ow01_2:WWSound_Ow01_2 = new WWSound_Ow01_2();
				soundChannel = ow01_2.play();
			}
		}
		
		public function playAhHardSound(... args):void {
			var random:Number = Math.random();
			var soundChannel:SoundChannel;
			if (voiceID == 1) {
				if (random <= 0.2) {
					var ahHard01:WWSound_AhHard01 = new WWSound_AhHard01();
					soundChannel = ahHard01.play();
				} else if (random <= 0.4) {
					var ahHard02:WWSound_AhHard02 = new WWSound_AhHard02();
					soundChannel = ahHard02.play();
				} else if (random <= 0.6) {
					var ahHard03:WWSound_AhHard03 = new WWSound_AhHard03();
					soundChannel = ahHard03.play();
				} else if (random <= 0.8) {
					var ahHard04:WWSound_AhHard04 = new WWSound_AhHard04();
					soundChannel = ahHard04.play();
				} else {
					var ahHard05:WWSound_AhHard05 = new WWSound_AhHard05();
					soundChannel = ahHard05.play();
				}
			} else {
				if (random <= 0.2) {
					var ahHard01_2:WWSound_AhHard01_2 = new WWSound_AhHard01_2();
					soundChannel = ahHard01_2.play();
				} else if (random <= 0.4) {
					var ahHard02_2:WWSound_AhHard02_2 = new WWSound_AhHard02_2();
					soundChannel = ahHard02_2.play();
				} else if (random <= 0.6) {
					var ahHard03_2:WWSound_AhHard03_2 = new WWSound_AhHard03_2();
					soundChannel = ahHard03_2.play();
				} else if (random <= 0.8) {
					var ahHard04_2:WWSound_AhHard04_2 = new WWSound_AhHard04_2();
					soundChannel = ahHard04_2.play();
				} else {
					var ahHard05_2:WWSound_AhHard05_2 = new WWSound_AhHard05_2();
					soundChannel = ahHard05_2.play();
				}
			}
		}
		
		public function playOhHardSound(... args):void {
			var random:Number = Math.random();
			var soundChannel:SoundChannel;
			if (voiceID == 1) {
				if (random <= 0.2) {
					var ohHard01:WWSound_OhHard01 = new WWSound_OhHard01();
					soundChannel = ohHard01.play();
				} else if (random <= 0.4) {
					var ohHard02:WWSound_OhHard02 = new WWSound_OhHard02();
					soundChannel = ohHard02.play();
				} else if (random <= 0.6) {
					var ohHard03:WWSound_OhHard03 = new WWSound_OhHard03();
					soundChannel = ohHard03.play();
				} else if (random <= 0.8) {
					var ohHard04:WWSound_OhHard04 = new WWSound_OhHard04();
					soundChannel = ohHard04.play();
				} else {
					var ohHard05:WWSound_OhHard05 = new WWSound_OhHard05();
					soundChannel = ohHard05.play();
				}
			} else {
				if (random <= 0.2) {
					var ohHard01_2:WWSound_OhHard01_2 = new WWSound_OhHard01_2();
					soundChannel = ohHard01_2.play();
				} else if (random <= 0.4) {
					var ohHard02_2:WWSound_OhHard02_2 = new WWSound_OhHard02_2();
					soundChannel = ohHard02_2.play();
				} else if (random <= 0.6) {
					var ohHard03_2:WWSound_OhHard03_2 = new WWSound_OhHard03_2();
					soundChannel = ohHard03_2.play();
				} else if (random <= 0.8) {
					var ohHard04_2:WWSound_OhHard04_2 = new WWSound_OhHard04_2();
					soundChannel = ohHard04_2.play();
				} else {
					var ohHard05_2:WWSound_OhHard05_2 = new WWSound_OhHard05_2();
					soundChannel = ohHard05_2.play();
				}
			}
		}
		
		public function playOhSoftSound(... args):void {
			var random:Number = Math.random();
			var soundChannel:SoundChannel;
			if (voiceID == 1) {
				if (random <= 0.2) {
					var ohSoft01:WWSound_OhSoft01 = new WWSound_OhSoft01();
					soundChannel = ohSoft01.play();
				} else if (random <= 0.4) {
					var ohSoft02:WWSound_OhSoft02 = new WWSound_OhSoft02();
					soundChannel = ohSoft02.play();
				} else if (random <= 0.6) {
					var ohSoft03:WWSound_OhSoft03 = new WWSound_OhSoft03();
					soundChannel = ohSoft03.play();
				} else if (random <= 0.8) {
					var ohSoft04:WWSound_OhSoft04 = new WWSound_OhSoft04();
					soundChannel = ohSoft04.play();
				} else {
					var ohSoft05:WWSound_OhSoft05 = new WWSound_OhSoft05();
					soundChannel = ohSoft05.play();
				}
			} else {
				if (random <= 0.2) {
					var ohSoft01_2:WWSound_OhSoft01_2 = new WWSound_OhSoft01_2();
					soundChannel = ohSoft01_2.play();
				} else if (random <= 0.4) {
					var ohSoft02_2:WWSound_OhSoft02_2 = new WWSound_OhSoft02_2();
					soundChannel = ohSoft02_2.play();
				} else if (random <= 0.6) {
					var ohSoft03_2:WWSound_OhSoft03_2 = new WWSound_OhSoft03_2();
					soundChannel = ohSoft03_2.play();
				} else if (random <= 0.8) {
					var ohSoft04_2:WWSound_OhSoft04_2 = new WWSound_OhSoft04_2();
					soundChannel = ohSoft04_2.play();
				} else {
					var ohSoft05_2:WWSound_OhSoft05_2 = new WWSound_OhSoft05_2();
					soundChannel = ohSoft05_2.play();
				}
			}
		}
		
		public function playGagSound(... args):void {
			checkInitClasses();
			var random:Number = Math.random();
			if (random <= 0.3) {
				playSound(g.soundControl.wretch1.play(), 0.5);
			} else if (random <= 0.6) {
				playSound(g.soundControl.wretch2.play(), 0.5);
			} else {
				playSound(g.soundControl.wretch3.play(), 0.5);
			}
			startGagRender();
		}
		
		private function playSound(sound:SoundChannel, value:Number):void {
			volume.volume = value;
			sound.soundTransform = volume;
		}
		
		private function startGagRender():void {
			if (gagging) {
				return;
			}
			gagging = true;
			g.her.head.neck.swallow();
			g.her.head.neck.swallowProgress = 0.94;
			gagFunctionObject = new FunctionObject(gagRender, this, []);
			m.mc.addEventListener(Event.ENTER_FRAME, gagFunctionObject.call);
		}
		
		public function gagRender(e:Event = null):void {
			g.her.head.neck.swallowProgress = Math.max(0, g.her.head.neck.swallowProgress - 0.08);
			if (g.her.head.neck.swallowProgress < 0.2) {
				g.her.head.neck.swallowProgress = 1;
				m.mc.removeEventListener(Event.ENTER_FRAME, gagFunctionObject.call);
				gagFunctionObject = null;
				gagging = false;
			}
		}
	}

}