package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard05_2.swf",symbol="WWSound_OhHard05_2")]
	
	public dynamic class WWSound_OhHard05_2 extends Sound {
		
		public function WWSound_OhHard05_2() {
			super();
		}
	}

}
