package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_AhHard01_2.swf",symbol="WWSound_AhHard01_2")]
	
	public dynamic class WWSound_AhHard01_2 extends Sound {
		
		public function WWSound_AhHard01_2() {
			super();
		}
	}

}
