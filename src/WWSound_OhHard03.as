package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard03.swf",symbol="WWSound_OhHard03")]
	
	public dynamic class WWSound_OhHard03 extends Sound {
		
		public function WWSound_OhHard03() {
			super();
		}
	}

}
