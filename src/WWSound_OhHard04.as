package {
	
	import flash.media.Sound;
	
	[Embed(source="../audio/WWSounds_OhHard04.swf",symbol="WWSound_OhHard04")]
	
	public dynamic class WWSound_OhHard04 extends Sound {
		
		public function WWSound_OhHard04() {
			super();
		}
	}

}
